- Needed classes:
    - Player
    - Game
    - Card
    - Bank extends Player
    - Render


Aufgaben bis zum nächsten Mal: 
- Welches DesignPattern für welche Klasse verwenden? 
- Struktur der Klassen definieren (keine Implementierungen, nötigenfalls Kommentare statt Implementierunge)

Ziel für nächstes Mal:
- Gemeinsames Verständnis für die Implementierung damit wir das erstellen der Klassen aufteilen können und am Ende die Klassen miteinander interagieren 

Mittel- Langfristige Features
- Multiplayer (Bank bleibt Bank und wird Serverseitig berechnet und interagiert über events mit dem Spiel)
- KI Spieler (eventuell in python) und KI Bank (Menschlich immitierende Verhaltensweisen)
- Boom Street!